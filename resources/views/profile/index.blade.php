@extends('layout.master')

@section('judul')
  Update Profile
@endsection

@section('content')
  <form action="/profil/{{$profile->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label>Umur profile</label>
      <input type="number" name="umur" value="{{$profile->umur}}" class="form-control">
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Biodata</label>
      <textarea name="bio" class="form-control">{{$profile->bio}}</textarea>
    </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Alamat</label>
      <textarea name="alamat" class="form-control">{{$profile->alamat}}</textarea>
    </div>
    @error('alamat')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection