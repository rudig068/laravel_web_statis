@extends('layout.master')

@section('judul')
    <h4>Halaman Form</h4>
@endsection

@section('content')
<h2>Buat Account Baru</h2>
<h3>Sign Up Form</h3>
<form action="/welcome" method="post">
  @csrf
  <label for="username">First name:</label><br /><br />
  <input type="text" name="first" /><br /><br />
  <label for="username">Last name:</label><br /><br />
  <input type="text" name="last" /><br /><br />
  <label>Gender</label> <br /><br />
  <input type="radio" name="jk" />Male<br />
  <input type="radio" name="jk" />Female<br /><br />
  <label>Nationality</label><br /><br />
  <select name="Nationality">
    <option value="Indonesia">Indonesia</option>
    <option value="malaysia">Malaysia</option>
    <option value="vietnam">Vietnam</option></select
  ><br /><br />
  <label>Language Spoken</label><br /><br />
  <input type="checkbox" />Bahasa Indonesia<br />
  <input type="checkbox" />English<br />
  <input type="checkbox" />Other<br /><br />
  <label>Bio</label><br /><br />
  <textarea name="message" cols="30" rows="10"></textarea>
  <br />
  <input type="submit" value="Sign Up" />
</form>
@endsection
