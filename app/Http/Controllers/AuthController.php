<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function bio(){
        return view('form');
    }

    public function send(Request $request){
        $first = $request['first'];
        $last = $request['last'];

        return view('home', compact('first','last'));
    }
}
