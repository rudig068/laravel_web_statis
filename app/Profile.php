<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profil';
    
    protected $fillable = ['umur', 'bio', 'alamat', 'users_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
