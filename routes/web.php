<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

Route::get('/register', 'AuthController@bio');

Route::post('/welcome', 'Authcontroller@send');

Route::get('/data-table', 'TableController@table');

// Crud cast
// create
Route::group(['middleware' => ['auth']], function () {
  Route::get('/cast/create', 'CastController@create'); //Route menuju form create
  Route::post('/cast', 'CastController@store'); //Route untuk menyimpan data ke database

  // Read
  Route::get('/cast', 'CastController@index'); //Route list Cast
  Route::get('/cast/{cast_id}', 'CastController@show'); //Route Detail cast

  // Update
  Route::get('/cast/{cast_id}/edit', 'CastController@edit'); //Route 
  Route::put('/cast/{cast_id}', 'CastController@update');

  // Delete
  Route::delete('/cast/{cast_id}', 'CastController@destroy');

  //profile
  Route::resource('profil', 'ProfileController')->only([
    'index', 'update'
  ]);
  
});

Auth::routes();